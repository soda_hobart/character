import Vue from 'vue'
import Vuex from 'vuex'
import {computeModifer, ability, crap} from './characterModel.js'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
	names: {
	    playerName: 'Sam',
	    characterName: ''
	},
	//must be an ability instance
	abilities: {
	    STR: new ability('Strength'),
	    DEX: new ability('Dexterity'),
	    CON: new ability('Constitution'),
	    INT: new ability('Intelligence'),
	    WIS: new ability('Wisdom'),
	    CHA: new ability('Charisma'),
	    total: 0,
	    currentTotal: 0,
	    unspent: 0
	},
	//must be a characterClasses object
	characterClass: null,
	//must contain equipment objects
	inventory: [],
	gold: 0,
	//equipped should allow only 1 armor & 1 weapon
	equipped: {
	    armor: {},
	    weapon: {}
	},
	//stats is computedValues instance 
	stats: {
	},
	attainment: {},
	spells: []
  },
    mutations: {
	setPlayerName (name) {
	    state.playerName = name
	},
	setCharacterName (name) {
	    state.characterName = name
	},
	setSTR (value) {
	    state.abilities.STR.abilityScore = value
	},
	setDEX (value) {
	    state.abilities.DEX.abilityScore = value
	},
	setCON (value) {
	    state.abilities.CON.abilityScore = value
	},
	setINT (value) {
	    state.abilities.INT.abilityScore = value
	},
	setWIS (value) {
	    state.abilities.WIS.abilityScore = value
	},
	setCHA (value) {
	    state.abilities.CHA.abilityScore = value
	},
	setTotal (state, value) {
	    state.abilities.total = value
	},
	setCurrentTotal (state, value) {
	    state.abilities.currentTotal = value
	},
	setUnspent (state, value) {
	    state.abilities.unspent = value
	},
	setCharacterClass (state, value) {
	    state.characterClass = value
	},
	setGold (state, value) {
	    state.gold = value
	},
	addInventory (state, value) {
	    state.inventory.push(value)
	},
	removeInventory (state, value) {
	    //removes using string representation as key
	    for (let i=0; i<state.inventory.length; i++)
	    {
		if (inventory[i].repr === value)
		{
		    inventory.splice(i, 1)
		    break
		}
	    }
	}

  },
  actions: {

  }
})
