
export function computeModifier (abilityScore) {
    var modifier = 0;
    if (abilityScore > 17) {
	modifier += 4
    }
    else if (abilityScore > 16) {
	modifier += 3
    }
    else if (abilityScore > 15) {
	modifier += 2
    }
    else if (abilityScore > 14) {
	modifier += 1
    }
    else if (abilityScore < 6) {
	modifier -= 1  
    }
    else if (abilityScore < 5) {
	modifier -= 2  
    }
    else if (abilityScore < 4) {
	modifier -= 3  
    }
    else if (abilityScore < 3) {
	modifier -= 4  
    }
    return modifier
}

export function ability (abilityName) {
    this.abilityName = abilityName
    this.abilityScore = Math.floor((Math.random() * 16) + 3)
    this.modifier = computeModifier(this.abilityScore)
}


export const characterClasses = {
    wizard: {
	className: "Wizard",
	baseTHAC0: Number(20),
	saveVsSpell: Number(14),
	saveVsTrap: Number(15),
	saveVsOoze: Number(18),
	baseHP: Number(4),
	baseGold: Number(40),
	prerequisites: "Needs INT of 9",
	description: "Uses magic & potions"
    },

    fighter: {
	className: "Fighter",
	baseTHAC0: Number(20),
	saveVsSpell: Number(19),
	saveVsTrap: Number(18),
	saveVsOoze: Number(14),
	baseHP: Number(10),
	baseGold: Number(100),
	prerequisites: "Needs STR of 9",
	description: "Bash things"
    },

    druid: {
	className: "Druid",
	baseTHAC0: Number(20),
	saveVsSpell: Number(17),
	saveVsTrap: Number(17),
	saveVsOoze: Number(17),
	baseHP: Number(8),
	baseGold: Number(40),
	prerequisites: "Needs WIS of 9",
	description: "Mysterious shaman"
    },
    
    thief: {
	className: "Thief",
	baseTHAC0: Number(20),
	saveVsSpell: Number(19),
	saveVsTrap: Number(14),
	saveVsOoze: Number(17),
	baseHP: Number(6),
	baseGold: Number(80),
	prerequisites: "Needs DEX of 9",
	description: "Takes stuff"
    }
}

export const equipment = {
    armor: {
	chainmail: {repr: 'chainmail', armorClass: Number(5), cost: 60},
	leather: {repr: 'leather', armorClass: Number(7), cost: 20},
	plate: {repr: 'plate', armorClass: Number(2), cost: 100},
	cloak: {repr: 'cloak', armorClass: Number(10), cost: 10}
    },
    weapon: {
	broadsword: {
	    repr: 'broad sword',
	    damage: Number(10),
	    cost: 30
	},
	morningstar: {
	    repr: 'morning star',
	    damage: Number(8),
	    cost: 20
	},
	dagger: {
	    repr: 'dagger',
	    damage: Number(6),
	    cost: 15
	},
	quarterstaff: {
	    repr: 'quarterstaff',
	    damage: Number(4),
	    cost: 5
	}
    }
}


export function computedValues (classObj, abilitiesObj, equippedObj) {
    values = {
	classValues: {
	    repr: 'Class',
	    value: classObj.className
	},
	THAC0Values: {
	    repr: 'THAC0',
	    value: classObj.baseTHAC0 - abilitiesObj.DEX.modifier
	},
	saveVsSpell: {
	    repr: 'Save vs. Spell',
	    value: classObj.saveVsSpell - abilitiesObj.INT.modifier
	},
	saveVsTrap: {
	    repr: 'Save vs. Trap',
	    value: classObj.saveVsTrap - abilitiesObj.DEX.modifier
	},
	saveVsOoze: {
	    repr: 'Save vs. Ooze',
	    value: classObj.saveVsTrap - abilitiesObj.CON.modifier
	},
	armorClass: {
	    repr: 'Armour Classe',
	    value: classObj.equippedObj.armor.armorClass - abilitiesObj.STR.modifier
	},
	hitPoints: {
	    repr: 'Hit Points',
	    value: classObj.baseHP + abilitiesObj.CON.modifier
	},
	damage: {
	    repr: 'Damage',
	    value: equippedObj.weapon.damage + abilitiesObj.STR.modifier
	}
    }
}
